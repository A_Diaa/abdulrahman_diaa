#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
struct book
{
	string title;
	double price;
};

book getExpensive(book[]);

int main()
{
	book books[5], max;
	for (int i = 0; i < 5; i++)
	{
		cin >> books[i].title >> books[i].price;
	}
	max = getExpensive(books);
	cout << max.title << endl;
	cout << fixed <<max.price << endl;
	system("pause");
	return 0;
}

book getExpensive(book books[])
{
	book max;
	max.price = 0.0;
	for (int i = 0; i < 5; i++)
		if (books[i].price > max.price)
			max = books[i];
	return max;
}