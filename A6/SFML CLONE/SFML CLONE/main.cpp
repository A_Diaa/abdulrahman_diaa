#include <time.h>
#include <stdlib.h>
#include <SFML/Graphics.hpp>
#include <string>

enum color {red, green, blue, yellow, white, dark, purple, cyan, orange, magenta}; //R G B Y W D P C O M

const int rows = 20, cols = rows;
const int side = 50;

int numColors = 8, size = 15;
int hits = 0;
float resizeFactorX = 1, resizeFactorY = 1;

sf::RenderWindow window;
sf::RenderWindow startup;
sf::RenderWindow result;
sf::Font font;


bool checkWin(color[][cols]);
void update2D(color[][cols], int, int, color current, color chosen);
void Display(color[][cols]);
void Fill(color[][cols]);
void handleEvent(color [][cols]);
void configure();
void updateStatus();
void resultt(bool, bool&);

int main()
{
	bool win ,restart;
	srand(time(NULL));
	font.loadFromFile("arial.ttf");

	do
	{
		hits = 0;
		restart = false;

		startup.create(sf::VideoMode(200, 400), "Menu");
		configure();

		color game[rows][cols];
		Fill(game);

		window.create(sf::VideoMode(size*side+200, size*side), "FLOOD IT!");
		while (window.isOpen())
		{
			window.clear();
			handleEvent(game); //game input
			Display(game); //render
			updateStatus();
			window.display(); //output

			if (hits > size*numColors/4)
			{
				win = false;
				window.close();
				result.create(sf::VideoMode(200, 200), "LOST");
			}

			if (checkWin(game))
			{
				win = true;
				window.close();
				result.create(sf::VideoMode(200, 200), "WON");
			}

		}

		resultt(win, restart);

	} while (restart);

	return 0;
}

void update2D (color game[][cols], int i, int j, color current, color chosen)
{
	if (i < 0 || i >= size || j < 0 || j >= size)
		return;
	if (game[i][j] == current)
	{
		game[i][j] = chosen;
		update2D(game, i + 1, j, current, chosen);
		update2D(game, i , j+1, current, chosen);
		update2D(game, i - 1, j, current, chosen);
		update2D(game, i, j - 1, current, chosen);
	}
}

void updateStatus()
{

	sf::RectangleShape consumed(sf::Vector2f(200, size*side));
	consumed.setPosition(size*side, 0);
	consumed.setOutlineThickness(-20);
	consumed.setOutlineColor(sf::Color::Black);
	consumed.setFillColor(sf::Color::Red);

	sf::RectangleShape remaining(sf::Vector2f(200, size*side-4*side*hits/numColors));
	remaining.setPosition(size*side, 0);
	remaining.setOutlineThickness(-20);
	remaining.setOutlineColor(sf::Color::Black);
	remaining.setFillColor(sf::Color::Green);

	sf::Text hitsT;
	hitsT.setString(std::to_string(hits));
	hitsT.setPosition(size*side+90, size*side/2);
	hitsT.setFillColor(sf::Color::Black);
	hitsT.setCharacterSize(30);
	hitsT.setStyle(sf::Text::Bold);
	hitsT.setFont(font);

	window.draw(consumed);
	window.draw(remaining);
	window.draw(hitsT);
}

void Display(color game[][cols]) //red, green, blue, yellow, white, dark, purple, cyan, orange, magenta
{
	sf::RectangleShape rectangle(sf::Vector2f(side, side));
	rectangle.setOutlineThickness(-5);
	rectangle.setOutlineColor(sf::Color::Black);

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			rectangle.setPosition(j*side, i*side);

			switch (game[i][j])
			{
			case red:
				rectangle.setFillColor(sf::Color::Red);
				break;
			case green:
				rectangle.setFillColor(sf::Color::Green);
				break;
			case blue:
				rectangle.setFillColor(sf::Color::Blue);
				break;
			case yellow:
				rectangle.setFillColor(sf::Color::Yellow);
				break;
			case white:
				rectangle.setFillColor(sf::Color::White);
				break;
			case dark:
				rectangle.setFillColor(sf::Color::Black);
				break;
			case purple:
				rectangle.setFillColor(sf::Color(51, 0, 51));
				break;
			case cyan:
				rectangle.setFillColor(sf::Color::Cyan);
				break;
			case orange:
				rectangle.setFillColor(sf::Color(255, 128, 0));
				break;
			case magenta:
				rectangle.setFillColor(sf::Color::Magenta);
				break;
			}
			window.draw(rectangle);
		}
	}
}

void Fill(color game[][cols])
{
	int temp;
	for (int i = 0; i < size; i++) 
		for (int j = 0; j < size; j++)
		{
			temp = rand() % numColors;
			game[i][j] = color(temp);
		}
}

void handleEvent(color game[][cols])
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed: //R G B Y W D P C O M
		{
			switch (event.key.code)
			{
			case sf::Keyboard::R:
				hits++;
				update2D(game, 0, 0, game[0][0], red);
				break;
			case sf::Keyboard::G:
				hits++;
				update2D(game, 0, 0, game[0][0], green);
				break;
			case sf::Keyboard::B:
				hits++;
				update2D(game, 0, 0, game[0][0], blue);
				break;
			case sf::Keyboard::Y:
				hits++;
				update2D(game, 0, 0, game[0][0], yellow);
				break;
			case sf::Keyboard::W:
				hits++;
				update2D(game, 0, 0, game[0][0], white);
				break;
			case sf::Keyboard::D:
				hits++;
				update2D(game, 0, 0, game[0][0], dark);
				break;
			case sf::Keyboard::P:
				hits++;
				update2D(game, 0, 0, game[0][0], purple);
				break;
			case sf::Keyboard::C:
				hits++;
				update2D(game, 0, 0, game[0][0], cyan);
				break;
			case sf::Keyboard::O:
				hits++;
				update2D(game, 0, 0, game[0][0], orange);
				break;
			case sf::Keyboard::M:
				hits++;
				update2D(game, 0, 0, game[0][0], magenta);
				break;
			}
			break;
		}

		case sf::Event::MouseButtonPressed:
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				int x = (event.mouseButton.x) / (float(side) * resizeFactorX);
				int y = (event.mouseButton.y) / (float(side) * resizeFactorY);
				if (x<size*side)
					if (game[y][x] != game[0][0])
				{
					hits++;
					update2D(game, 0, 0, game[0][0], game[y][x]);
				}
			}
			break;
		}
		case sf::Event::Resized:
		{
			resizeFactorX = float(event.size.width) / float(size*side+200);
			resizeFactorY = float(event.size.height) / float(size*side);
			break;
		}
		case sf::Event::Closed:
			window.close();
			break;
		}
	}
}

bool checkWin(color game[][cols])
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			if (game[i][j] != game[0][0])
				return false;
	return true;
}

void configure()
{
	int mouseX, mouseY;
	const int sizee = 100;

	sf::Text colorT1;
	colorT1.setString("C:Easy");
	colorT1.setPosition(20, 40);
	colorT1.setFillColor(sf::Color::Black);
	colorT1.setCharacterSize(18);
	colorT1.setStyle(sf::Text::Bold);
	colorT1.setFont(font);

	sf::Text colorT2;
	colorT2.setString("C:Medium");
	colorT2.setPosition(5, 140);
	colorT2.setFillColor(sf::Color::Black);
	colorT2.setCharacterSize(17);
	colorT2.setStyle(sf::Text::Bold);
	colorT2.setFont(font);

	sf::Text colorT3;
	colorT3.setString("C:Hard");
	colorT3.setPosition(20, 240);
	colorT3.setFillColor(sf::Color::Black);
	colorT3.setCharacterSize(18);
	colorT3.setStyle(sf::Text::Bold);
	colorT3.setFont(font);

	sf::Text sizeT1;
	sizeT1.setString("S:Easy");
	sizeT1.setPosition(120, 40);
	sizeT1.setFillColor(sf::Color::Black);
	sizeT1.setCharacterSize(18);
	sizeT1.setStyle(sf::Text::Bold);
	sizeT1.setFont(font);

	sf::Text sizeT2;
	sizeT2.setString("S:Medium");
	sizeT2.setPosition(105, 140);
	sizeT2.setFillColor(sf::Color::Black);
	sizeT2.setCharacterSize(17);
	sizeT2.setStyle(sf::Text::Bold);
	sizeT2.setFont(font);

	sf::Text sizeT3;
	sizeT3.setString("S:Hard");
	sizeT3.setPosition(120, 240);
	sizeT3.setFillColor(sf::Color::Black);
	sizeT3.setCharacterSize(18);
	sizeT3.setStyle(sf::Text::Bold);
	sizeT3.setFont(font);

	sf::Text okayT;
	okayT.setString("Okay");
	okayT.setPosition(125, 340);
	okayT.setFillColor(sf::Color::Black);
	okayT.setCharacterSize(19);
	okayT.setStyle(sf::Text::Bold);
	okayT.setFont(font);

	sf::Text cheatT;
	cheatT.setString("Baby");
	cheatT.setPosition(25, 340);
	cheatT.setFillColor(sf::Color::Black);
	cheatT.setCharacterSize(19);
	cheatT.setStyle(sf::Text::Bold);
	cheatT.setFont(font);

	sf::RectangleShape color1(sf::Vector2f(sizee, sizee));
	color1.setPosition(0, 0);
	color1.setOutlineThickness(-5);
	color1.setOutlineColor(sf::Color::Black);
	color1.setFillColor(sf::Color::Green);

	sf::RectangleShape color2(sf::Vector2f(sizee, sizee));
	color2.setPosition(0, sizee);
	color2.setOutlineThickness(-5);
	color2.setOutlineColor(sf::Color::Black);
	color2.setFillColor(sf::Color::Yellow);

	sf::RectangleShape color3(sf::Vector2f(sizee, sizee));
	color3.setPosition(0, 2 * sizee);
	color3.setOutlineThickness(-5);
	color3.setOutlineColor(sf::Color::Black);
	color3.setFillColor(sf::Color::Red);
	
	sf::RectangleShape size1(sf::Vector2f(sizee, sizee));
	size1.setPosition(sizee, 0);
	size1.setOutlineThickness(-5);
	size1.setOutlineColor(sf::Color::Black);
	size1.setFillColor(sf::Color::Green);

	sf::RectangleShape size2(sf::Vector2f(sizee, sizee));
	size2.setPosition(sizee, sizee);
	size2.setOutlineThickness(-5);
	size2.setOutlineColor(sf::Color::Black);
	size2.setFillColor(sf::Color::Yellow);

	sf::RectangleShape size3(sf::Vector2f(sizee, sizee));
	size3.setPosition(sizee, 2*sizee);
	size3.setOutlineThickness(-5);
	size3.setOutlineColor(sf::Color::Black);
	size3.setFillColor(sf::Color::Red);

	sf::RectangleShape okay (sf::Vector2f(sizee, sizee));
	okay.setPosition(sizee, 3 * sizee);
	okay.setOutlineThickness(-5);
	okay.setOutlineColor(sf::Color::Black);
	okay.setFillColor(sf::Color::White);

	sf::RectangleShape cheat(sf::Vector2f(sizee, sizee));
	cheat.setPosition(0, 3 * sizee);
	cheat.setOutlineThickness(-5);
	cheat.setOutlineColor(sf::Color::Black);
	cheat.setFillColor(sf::Color::Magenta);


	while (startup.isOpen())
	{
		startup.clear();
		sf::Event event;
		while (startup.pollEvent(event))
		{
			if (event.type==sf::Event::MouseButtonPressed)
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					startup.clear();
					mouseX = event.mouseButton.x / sizee;
					mouseY = event.mouseButton.y / sizee;
					switch (mouseX)
					{
					case 0: 
					{
						switch (mouseY)
						{
						case 0: 
							numColors = 6;
							color1.setFillColor(sf::Color::Cyan);
							color2.setFillColor(sf::Color::Yellow);
							color3.setFillColor(sf::Color::Red);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 1: 
							numColors = 8;
							color2.setFillColor(sf::Color::Cyan);
							color1.setFillColor(sf::Color::Green);
							color3.setFillColor(sf::Color::Red);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 2: 
							numColors = 10;
							color3.setFillColor(sf::Color::Cyan);
							color1.setFillColor(sf::Color::Green);
							color2.setFillColor(sf::Color::Yellow);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 3: 
							size = 5;
							numColors = 4;

							size1.setFillColor(sf::Color::Green);
							size2.setFillColor(sf::Color::Yellow);
							size3.setFillColor(sf::Color::Red);
							color1.setFillColor(sf::Color::Green);
							color2.setFillColor(sf::Color::Yellow);
							color3.setFillColor(sf::Color::Red);
							cheat.setFillColor(sf::Color::Cyan);
							cheatT.setString("aww:(");
							break;
						}
						break;
					}
					case 1: 
					{
						switch (mouseY)
						{
						case 0:
							size = 10;
							size1.setFillColor(sf::Color::Cyan);
							size2.setFillColor(sf::Color::Yellow);
							size3.setFillColor(sf::Color::Red);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 1:
							size=15;
							size2.setFillColor(sf::Color::Cyan);
							size1.setFillColor(sf::Color::Green);
							size3.setFillColor(sf::Color::Red);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 2:
							size=20;
							size3.setFillColor(sf::Color::Cyan);
							size1.setFillColor(sf::Color::Green);
							size2.setFillColor(sf::Color::Yellow);
							cheat.setFillColor(sf::Color::Magenta);
							cheatT.setString("Baby");
							break;
						case 3:
							startup.close();
						}
						break;
					}
					}
				}
			if (event.type == sf::Event::Closed)
				startup.close();
		}
		startup.clear();
		startup.draw(color1);
		startup.draw(color2);
		startup.draw(color3);
		startup.draw(size1);
		startup.draw(size2);
		startup.draw(size3);
		startup.draw(okay);
		startup.draw(cheat);
		startup.draw(colorT1);
		startup.draw(colorT2);
		startup.draw(colorT3);
		startup.draw(sizeT1);
		startup.draw(sizeT2);
		startup.draw(sizeT3);
		startup.draw(okayT);
		startup.draw(cheatT);
		startup.display();

	}
}

void resultt(bool win, bool &restart)
{
	sf::Text stateT;
	stateT.setPosition(60, 40);
	stateT.setFillColor(sf::Color::Black);
	stateT.setCharacterSize(18);
	stateT.setStyle(sf::Text::Bold);
	stateT.setFont(font);

	sf::Text restartT;
	restartT.setPosition(15, 140);
	restartT.setString("RESTART");
	restartT.setFillColor(sf::Color::Black);
	restartT.setCharacterSize(14);
	restartT.setStyle(sf::Text::Bold);
	restartT.setFont(font);

	sf::Text dontT;
	dontT.setPosition(125, 140);
	dontT.setString("CLOSE");
	dontT.setFillColor(sf::Color::Black);
	dontT.setCharacterSize(14);
	dontT.setStyle(sf::Text::Bold);
	dontT.setFont(font);

	sf::RectangleShape state(sf::Vector2f(200, 100));
	state.setPosition(0, 0);
	state.setOutlineThickness(-20);
	state.setOutlineColor(sf::Color::Black);

	sf::RectangleShape restartt(sf::Vector2f(100, 100));
	restartt.setPosition(0, 100);
	restartt.setOutlineThickness(-10);
	restartt.setOutlineColor(sf::Color::Black);
	restartt.setFillColor(sf::Color::Blue);

	sf::RectangleShape dont(sf::Vector2f(100, 100));
	dont.setPosition(100, 100);
	dont.setOutlineThickness(-10);
	dont.setOutlineColor(sf::Color::Black);
	dont.setFillColor(sf::Color::Red);

	while (result.isOpen())
	{
		result.clear();
		sf::Event event;
		while (result.pollEvent(event))
		{
			if (event.type == sf::Event::MouseButtonPressed)
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					int x = (event.mouseButton.x) / 100.0;
					int y = (event.mouseButton.y) / 100.0;
					switch (y)
					{
					case 1:
						switch (x)
						{
						case 0:
							restart = true;
							result.close();
							break;
						case 1:
							restart = false;
							result.close();

							break;
						}
						break;
					}
				}
			if (event.type == sf::Event::Closed)
			{
				result.close();
			}

		}


		if (win)
		{
			state.setFillColor(sf::Color::Green);
			stateT.setString("YOU WIN!");
		}

		else
		{
			state.setFillColor(sf::Color::Red);
			stateT.setString("YOU lose...");
		}

		result.clear();
		result.draw(dont);
		result.draw(dontT);
		result.draw(state);
		result.draw(stateT);
		result.draw(restartt);
		result.draw(restartT);
		result.display();
	}
}