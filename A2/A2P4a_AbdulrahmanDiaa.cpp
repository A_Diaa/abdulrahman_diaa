#include <iostream>
#include <string>
using namespace std;
bool found(string, string[], int);
void fill(string [], int);
void sort(string [], int);
void swap(string &, string &);
void main()
{
	const int size = 10;
	string D[size], word, repeat;
	fill(D, size);
	sort(D, size);
	do
	{
		cout << "___________________________" << endl;
		cout << "now enter your search query: ";
		getline(cin, word);
		if (found(word, D, size))
			cout << "found" << endl;
		else
			cout << "not found" << endl;
		cout << "repeat? (Y/N)" << endl;
		getline(cin, repeat);
	} while (repeat != "N");	
	system("pause");
}
bool found(string w, string D[], int size)
{
	int l = 0, h = size-1, m, c;
	while ((l<=h)&&(h<size)&&(l>=0))
	{
		m = (l + h) / 2;
		c = w.compare(D[m]);
		if (c==0)
			return true;
		else if (c<0)
			h = m-1;
		else
			l = m+1;
	}
	return false;
}
void fill(string D[], int size)
{
	cout << "Enter " << size << " elements to fill the dictionary" << endl;
	for (int i = 0; i < size; i++)
		getline(cin, D[i]);
}
void sort(string D[], int size)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size -1- i; j++)
			if (D[j].compare(D[j+1])>0)
				swap(D[j], D[j + 1]);
}
void swap(string & a, string & b)
{
	string temp;
	temp = a;
	a = b;
	b = temp;
}