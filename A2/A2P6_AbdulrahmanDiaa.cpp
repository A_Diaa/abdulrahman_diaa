#include <iostream>
#include <string>
using namespace std;
string removeDuplicates(string);
void main()
{
	string word, repeat;
	do
	{
		cout << "enter word to remove duplicates: ";
		getline(cin, word);
		cout << "word after duplicates removed: ";
		cout << removeDuplicates(word) << endl;
		cout << "Repeat? (Y/N): ";
		getline(cin, repeat);
	} while (repeat != "N");
	system("pause");
}
string removeDuplicates(string w)
{
	int found;
	for (int i = 0; i < w.length(); i++)
	{
		found = i+1;;
		while (i<w.length())
		{
			found = w.find(w.at(i), found);
			if ((found == string::npos))
				break;
			else
				w.erase(found, 1);
		}
	}
	return w;
}