#include <iostream>
#include <string>
using namespace std;
bool found(string, string[], int);
bool doublet(string[], int, string, string);
void fill(string[], int);
void sort(string[], int);
void swap(string &, string &);
void main()
{
	const int size = 5;
	string D[size], w1, w2, repeat;
	fill(D, size);
	sort(D, size);
	do
	{
		cout << "___________________________" << endl;
		cout << "Now enter 2 words on separate lines to check if they are doublets: " << endl;
		getline(cin, w1);
		getline(cin, w2);
		if (doublet(D, size, w1, w2))
			cout << "doublets!!" << endl;
		else
			cout << "not doublets..." << endl;
		cout << "repeat? (Y/N)" << endl;
		getline(cin, repeat);
	} while (repeat != "N");
	system("pause");
}
bool found(string w, string D[], int size)
{
	int l = 0, h = size - 1, m, c;
	while ((l <= h) && (h<size) && (l >= 0))
	{
		m = (l + h) / 2;
		c = w.compare(D[m]);
		if (c == 0)
			return true;
		else if (c<0)
			h = m - 1;
		else
			l = m + 1;
	}
	return false;
}
bool doublet(string D[], int size, string w1, string w2)
{
	if (found(w1, D, size) && found(w2, D, size))
	{
		if (w1.length() == w2.length())
		{
			int c = 0;
			for (int i = 0; i < w1.length(); i++)
			{
				if (w1.at(i) != w2.at(i))
					c++;
			}
			if (c == 1)
				return true;
			else return false;
		}
		else return false;
	}
	else
	{
		cout << "one or both words not found in dictionary" << endl;
		return false;
	}
}
void fill(string D[], int size)
{
	cout << "Enter " << size << " elements to fill the dictionary" << endl;
	for (int i = 0; i < size; i++)
		getline(cin, D[i]);
}
void sort(string D[], int size)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size - 1 - i; j++)
			if (D[j].compare(D[j + 1])>0)
				swap(D[j], D[j + 1]);
}
void swap(string & a, string & b)
{
	string temp;
	temp = a;
	a = b;
	b = temp;
}