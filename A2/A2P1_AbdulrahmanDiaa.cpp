#include <iostream>
#include <string>
using namespace std;
void deleteAfter(string&, string&);
void main()
{
	string again;
	do
	{
		string s1, s2;
		getline(cin, s1);
		getline(cin, s2);
		cout << "_________________" << endl;
		deleteAfter(s1, s2);
		cout << s1 << endl;
		cout << "again? (Y/N) ";
		getline(cin, again);
		cout << "******************" << endl;
	} while (again != "N");
	system("pause");

}
void deleteAfter(string &s1, string& s2)
{
	if(s1.find(s2)!= string::npos)
		s1.erase((s1.find(s2) + s2.size()), string::npos);
}