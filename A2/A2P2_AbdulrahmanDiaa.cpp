#include <iostream>
#include <string>
using namespace std;
void flip(string&, string&);
void swap(string&, string, string);
void main()
{
	string str, s1, s2;
	cout << "enter a sentence here: ";
	getline(cin, str);
	cout << "now enter 2 words separated by a line: " << endl;
	getline(cin, s1);
	getline(cin, s2);
	swap(str, s1, s2);
	cout << "your sentence after swapping: ";
	cout << str << endl;
	system("pause");
}
void swap(string& str, string substr1, string substr2)
{
	if ((str.find(substr1) != string::npos) && (str.find(substr2) != string::npos))
	{
		if (str.find(substr1) < str.find(substr2))
		{
			flip(substr1, substr2);
		}
		str.replace(str.find(substr1), substr1.length(), substr2);
		str.replace(str.find(substr2), substr2.length(), substr1);
	}
}
void flip(string &a, string &b)
{
	string temp;
	temp = a;
	a = b;
	b = temp;
}