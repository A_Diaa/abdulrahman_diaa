#include <iostream>
#include <string>
using namespace std;
void StrSep(string, string, string&, string&);
void main ()
{
	string str, sep, out1, out2;
	cout << "enter a string: ";
	getline(cin, str);
	cout << "now enter a separator string: ";
	getline(cin, sep);
	StrSep(str, sep, out1, out2);
	cout << "your string after separation is: " << endl;
	cout << out1 << endl;
	cout << out2 << endl;
	system("pause");
}
void StrSep(string base, string sep, string& p1, string &p2)
{
	if (base.find(sep) != string::npos)
	{
		p1.assign(base, 0, base.find(sep));
		p2.assign(base, base.find(sep) + sep.length(), string::npos);
	}
}