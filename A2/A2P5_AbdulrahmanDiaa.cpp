#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
int wordcount(string, string);
void main()
{
	string str, word;
	cout << "Large string: ";
	getline(cin, str);
	cout << "Word to be counted: ";
	getline(cin, word);
	cout << "Wordcount= ";
	cout << setw(3)<<wordcount(word, str) << endl;
	system("pause");
}
int wordcount(string w, string s)
{
	int c = 0, found=-1;
	while (1)
	{
		found = s.find(w, found+1);
		if (found != string::npos)
			c++;
		else
			break;
	}
	return c;
}