#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;

enum color {black, white};
const int cols =5, rows=5;

bool compare(color[][cols], color [][cols], int);
void fill(color[][cols], int);
void display(color[][cols], int);

int main()
{
	srand(time(NULL));
	color A[rows][cols], B[rows][cols];
	fill(A, rows);
	fill(B, rows);
	display(A, rows);
	cout<< endl;
	display(B, rows);
	cout << endl;
	if (compare(A, B, rows))
		cout << "True" << endl;
	else
		cout << "False" << endl;
	system("pause");
	return 0;
}

bool compare(color A [] [cols], color B [] [cols], int rows)
{
	int wh1=0, wh2=0;
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
		{
			if (A[i][j])
				wh1++;
			if (B[i][j])
				wh2++;
		}

	return wh1==wh2;
}

void fill(color A[][cols], int rows)
{
	int k;
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
		{
			k = rand() % 2;
			if (k)
				A[i][j] = white;
			else
				A[i][j] = black;
		}
}

void display(color A [][cols], int rows)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			cout << setw(3) << A[i][j];
		cout << endl;
	}
}