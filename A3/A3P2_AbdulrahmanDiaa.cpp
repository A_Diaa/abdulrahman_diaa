#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;

const int cols = 10, rows = 10; //for resizing the game

enum type {water, land, bridge, cat};
enum result {starved, drowned, killed, survived };
enum direction {north, east, south, west };

void createGame(type[][cols], int);
void putMouse(type[][cols], int, int&, int&);
void putCat(type[][cols], int);
void showGame(type[][cols], int, int, int);
void moveMouse(int&, int&);

result playGame(type[][cols], int, int, int);

int main()
{
	int rno, cno;  //Column No and Row No of mouse
	int starvations=0, drowns=0, kills=0, survives=0; //counters for status
	int reps;

	type game[rows][cols];
	result res;

	srand(time(NULL));
	createGame(game, rows);
	putCat(game, rows);
	reps = (rand() % 20)+10;

	for (int i = 0; i < reps; i++)
	{
		cout << "Game " << i + 1 << " : " << endl;
		putMouse(game, rows, rno, cno);
		res = playGame(game, rows, rno, cno);
		if (res == survived)
			survives++;
		else if ((res == killed))
			kills++;
		else if ((res == drowned))
			drowns++;
		else if ((res == starved))
			starvations++;
	}

	cout << "Total: " << reps << endl;
	cout << "Survives: " << survives << endl;
	cout << "Kills: " << kills << endl;
	cout << "Starvations: " << starvations << endl;
	cout << "Drowns: " << drowns << endl;

	system("pause");
	return 0;
}

void createGame(type game[][cols], int rows)
{
	for (int i = 0; i<rows; i++)
		for (int j = 0; j < cols; j++)
		{
			if ((i == rows / 2) && (j == cols-1))
				game[i][j] = bridge;
			else if ((i == 0) || (j == 0) || (i == rows - 1) || (j == rows - 1))
				game[i][j] = water;
			else
				game[i][j] = land;
		}
}

void putMouse(type game[][cols], int rows, int& i, int& j)
{
	do
	{
		i = rand()%rows;
		j = rand()%cols;
	} while (game[i][j]!=land);
}

void putCat(type game [][cols], int rows)
{
	int i, j;
	do
	{
		i = rand()%rows;
		j = rand()%cols;
		if (game[i][j] == land)
			game[i][j]=cat;
	} while (game[i][j]!= cat);
}

void showGame(type game[][cols], int rows, int rno, int cno)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			if ((i == rno) && (j == cno))
				cout << setw(3) << 'J';
			else if (game[i][j] == land)
				cout << setw(3) << '_';
			else if (game[i][j] == water)
				cout << setw(3) << 'O';
			else if (game[i][j] == bridge)
				cout << setw(3) << '|';
			else
				cout << setw(3) << 'T';
		cout << endl;
	}
}

void moveMouse(int& rno, int& cno)
{
	int temp = rand() % 4;
	direction head = direction(temp);
	switch (head)
	{
	case north:
	{
		rno--;
		break;
	}
	case east:
	{
		cno++;
		break;
	}
	case south:
	{
		rno++;
		break;
	}
	case west:
	{
		cno--;
		break;
	}
	}
}

result playGame(type game [][cols], int rows, int rno, int cno)
{
	result result;
	bool alive = true;
	int moves = 0;
	cout << "starting positions: " << endl;
	showGame(game, rows, rno, cno);
	cout << endl;
	while (alive)
	{
		if (moves > 100)
		{
			result = starved;
			alive = false;
		}
		if (game[rno][cno] == land)
		{
			moveMouse(rno, cno);
			moves++;
		}
		if (game[rno][cno] == cat)
		{
			alive = false;
			result = killed;
		}
		if (game[rno][cno] == water)
		{
			alive = false;
			result = drowned;
		}
		if (game[rno][cno] == bridge)
		{
			result = survived;
			break;
		}
	}
	cout << "Final positions: " << endl;
	showGame(game, rows, rno, cno);
	cout << endl;
	return result;
}