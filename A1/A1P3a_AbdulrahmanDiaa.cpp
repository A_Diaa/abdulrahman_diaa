#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	for (int i = 2; i <= 5; i++)
	{
		for (int c = i; c <= 5 * i; c += i)
			cout << setw(3) << c;
		cout << endl;
	}
	system("pause");
}
