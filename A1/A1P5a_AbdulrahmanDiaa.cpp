#include <iostream>
#include <cmath>
using namespace std;
bool prime(int);
void main()
{
	int num;
	char quit;
	do 
	{
		cin >> num;
		if (prime(num))
			cout << "prime number" << endl;
		else
			cout << "not prime number" << endl;
		cout << "Quit ? (Y/N) " << endl;
		cin >> quit;
	} while ((quit != 'y') && (quit != 'Y'));
	system("pause");
}
bool prime(int a)
{
	if ((a == 0) || (a == 1))
		return false;
	int c = 2;
	while (c <= sqrt(a))
		if (a%c == 0)
		{
			return false;
		}
		else
			c++;
	return true;
}