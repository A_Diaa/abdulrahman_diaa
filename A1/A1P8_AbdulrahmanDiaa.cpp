#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;
void fill(int[], int);
void arrangeD(int[], int);
void swap(int&, int&);
void display(int[], int);
void main()
{
	const int size = 20; 
	int arr[size];
	fill(arr, size);
	cout << "Unsorted balls represented by 0 and 1: " <<endl;
	display(arr, size);
	arrangeD(arr, size);
	cout << "sorted balls represented by 0 and 1: " << endl;
	display(arr, size);
	system("pause");
}
void fill(int a[], int size)
{
	for (int i = 0; i < size; i++)
		a[i] = i % 2;
}
//0 for black and 1 for white
void arrangeD(int arr[], int size)
{
	for (int i = 0; i < (size / 2); i++)
		for (int c = i + 1; c < size - 1 - i; c += 2)
			swap(arr[c], arr[c + 1]);
}
void swap(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}
void display(int arr[], int size)
{
	for (int i = 0; i < size; i++)
		cout << setw(3) << arr[i];
	cout << endl;
}