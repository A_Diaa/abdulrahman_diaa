#include <iostream>
#include <string>
using namespace std;
int main()
{
	int period;
	float cost, total;
	const float P = 125.00, S = 95.00, W = 75.00, TV = 3.50, TEL = 1.75;
	char roomT, tel, tv;
	string input;
	do
	{
		cout << "Enter room data please: ";
		cin >> period >> input;
	} while ((period <= 0) || (input.length() != 3));
	roomT = input.at(0);
	tel = input.at(1);
	tv = input.at(2);
	cost = 0;
	switch (roomT)
	{
	case 'P':
	case 'p':
	{
		cost += P;
		break;
	}
	case 's':
	case 'S':
	{
		cost += S;
		break;
	}
	case'w':
	case'W':
	{
		cost += P;
		break;
	}
	default:
	{
		cout << "invalid room Type input" << endl;
		system("pause");
		return 0;
	}
	}
	if ((tel == 'Y') || (tel == 'y'))
		cost += TEL;
	if ((tv == 'Y') || (tv == 'y'))
		cost += TV;
	total = cost*period;
	cout << "Stay duration : " << period << " days." << endl;
	cout << "Room preference : " << roomT << endl;
	cout << "TV preferece: " << tv << endl;
	cout << "Telephone preference: " << tel << endl;
	cout << "Daily cost: " << cost << endl;
	cout << "Total amount due: " << total << endl;
	system("pause");
}
