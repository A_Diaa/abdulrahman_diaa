#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
using namespace std;
void fill(int[], int);
void sort(int[], int);
void swap(int&, int&);
void display(int[], int);
void main()
{
	const int size = 20;
	int arr[size];
	srand(unsigned(time(NULL)));
	fill(arr, size);
	cout << "Unsorted Random: " << endl;
	display(arr, size);
	sort(arr, size);
	cout << "After sorting: " << endl;
	display(arr, size);
	system("pause");
}
void fill(int a[], int size)
{
	for (int i = 0; i < size; i++)
		a[i] = rand() % size;
}
void sort(int arr[], int size)
{
	for (int i = 0; i < size-1; i++)
		for (int j = 0; j < size-1 - i; j++)
			if (arr[j] > arr[j + 1])
				swap(arr[j], arr[j + 1]);
}
void swap(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}
void display(int arr[], int size)
{
	for (int i = 0; i < size; i++)
		cout << setw(3) << arr[i];
	cout << endl;
}