#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	for (int i = 0; i < 6; i++)
	{
		for (int c = 0; c <= i; c++)
			if (i % 2)
				cout << setw(2) << '$';
			else
				cout << setw(2) << c + 1;
		cout << endl;
	}
	system("pause");
}
