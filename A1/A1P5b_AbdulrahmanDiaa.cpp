#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
bool prime(int);
void pNums(int[], int&);
void main()
{
	int arr[1000], index=0;
	pNums(arr, index);
	for (int i = 0; i < index; i++)
		cout << setw(4) << arr[i];
	cout << endl;
	system("pause");
}
bool prime(int a)
{
	int c = 2;
	while (c <= sqrt(a))
		if (a%c == 0)
		{
			return false;
		}
		else
			c++;
	return true;
}
void pNums(int a[], int & index)
{
	index = 0;
	for (int i=2; i<=1000; i++)
		if (prime(i))
		{
			a[index] = i;
			index++;
		}
}