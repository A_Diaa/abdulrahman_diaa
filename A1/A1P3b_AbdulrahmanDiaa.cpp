#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	for (int i = 0; i < 6; i++)
	{
		for (int c = 0; c < 6; c++)
			if (c == i)
				cout << setw(2) << '1';
			else
				cout << setw(2) << '0';
		cout << endl;
	}
	system("pause");
}
