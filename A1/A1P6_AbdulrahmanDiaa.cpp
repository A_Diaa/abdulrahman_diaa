#include <iostream>
#include <iomanip>
using namespace std;
void fill(int[]);
void main()
{
	int arr[50], j=0;
	fill(arr);
	for (int i = 0; i < 5; i++)
	{
		for (int c = 0; c < 10; c++)
		{
			cout << setw(5) <<arr[j];
			j++;
		}
		cout << endl;		
	}
	system ("pause");
}
void fill(int a[])
{
	for (int i = 0; i < 50; i++)
		if (i < 25)
			a[i] = i*i;
		else
			a[i] = 3 * i;
}