#include <iostream>
using namespace std; 

const int rows = 5; 
const int cols = 5;

void fill2D(int param [] [cols], int rows); 
int * turnTo1D(int param[][cols], int rows);
void display(int *);


int main()
{
	int param[rows][cols]; 
	int *pArr; 
	fill2D(param, rows); 
	pArr = turnTo1D(param, rows); 
	cout << endl;
	display(pArr); 
	system("pause");
	return 0; 
}

void fill2D(int param[][cols], int rows)
{
	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
			cin >> param[i][j]; 
}

int * turnTo1D(int param[][cols], int rows)
{
	int *temp = new int[rows*cols];

	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
			*(temp + i*cols + j) = param[i][j];

	return temp;
}

void display(int * pArr)
{
	for (unsigned int i = 0; i < rows*cols; i++)
		cout << *(pArr + i) << endl;
}