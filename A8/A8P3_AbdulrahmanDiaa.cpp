#include <iostream>
using namespace std;

const unsigned int rows = 5;
const unsigned int cols = 5;

void fill2D(char param[][cols], int rows);
char * turnTo1D(char param[][cols], int rows);
void display(char *);


int main()
{
	char param[rows][cols];
	char *pArr;
	fill2D(param, rows);
	pArr = turnTo1D(param, rows);
	cout << endl;
	display(pArr);
	system("pause");
	return 0;
}

void fill2D(char param[][cols], int rows)
{
	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
			cin >> param[i][j];
}

char * turnTo1D(char param[][cols], int rows)
{
	int c = 0;
	char *temp = new char[(rows*cols-cols)/2];

	for (unsigned int i = 0; i < rows; i++)
		for (unsigned int j = 0; j < cols; j++)
			if (j > i)
			{
				*temp++ = param[i][j];
				c++;
			}
	temp -= c;
	return temp;
}

void display(char * pArr)
{
	for (unsigned int i = 0; i < ( rows*cols - cols) / 2; i++)
		cout << *(pArr + i) << endl;
}