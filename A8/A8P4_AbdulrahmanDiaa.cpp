#include <iostream>
#include <string>
using namespace std; 

struct node
{
	char data; 
	node * next;
};

void fillList(node *&);
void displayList(node *&);

int main()
{
	node* head;
	head = NULL;
	fillList(head); 
	cout << endl;
	displayList(head);
	system("pause");
	return 0;
}

void fillList(node *& head)
{
	string temp; 
	cin >> temp;

	node * cursor;

	head = new node;
	head->data = temp.at(0); 
	head->next = NULL;
	cursor = head;

	for (unsigned int i = 1; i < temp.length(); i++)
	{
		cursor->next = new node; 
		cursor->next->data = temp.at(i); 
		cursor->next->next = NULL;
		cursor = cursor->next;
	}
}

void displayList(node *& head)
{
	node * cursor;
	cursor = head;

	while (cursor != NULL)
	{
		cout << cursor->data << endl;
		cursor = cursor->next;
	}

}