#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void fillVector(vector <string> &);
void removeDuplicates(vector <string> &);
void displayVector(const vector <string> &);

int main()
{
	vector <string> param;
	fillVector(param); 
	removeDuplicates(param); 
	cout << endl;
	displayVector(param);
	system("pause");
	return 0; 
}

void fillVector(vector <string> &param)
{
	int num;
	string temp;
	cin >> num; 

	for (int i = 0; i < num; i++)
	{
		cin >> temp;
		param.push_back(temp);
	}
}

void removeDuplicates(vector <string> & param)
{
	for (unsigned int i=0; i< param.size(); i++)
		for (unsigned int j = i+1; j < param.size(); j++)
		{
			if (param.at(i) == param.at(j))
				param.erase(param.begin() + j);
		}
}

void displayVector(const vector <string> & param)
{
	for (unsigned int i = 0; i < param.size(); i++)
		cout << param.at(i) << endl;
}