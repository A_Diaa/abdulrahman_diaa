#include <iostream>
using namespace std;

struct node
{
	int data; 
	node * next;
	node * back;
};

bool createL(node * &cursor, int size, int k); 
bool deleteReverse(node * cursor, int k);
void displayList(node *&);
void deleteHead(node *& head);


int main()
{
	int n, k;
	node * head = new node;
	head->back = NULL;
	cin >> n >> k;
	if (createL(head, n, k))
		deleteHead(head);
	displayList(head);
	system("pause");
	return 0;
}

bool createL(node *& cursor, int size, int k)
{
		cursor->data = rand() % 100 + 1;
		if (size > 1)
		{
			cursor->next = new node;
			cursor->next->next = NULL;
			cursor->next->back = cursor;
			createL(cursor->next, size - 1, k);
		}
		else
			return deleteReverse(cursor, k);
}

bool deleteReverse(node * cursor, int k)
{
	if (!cursor == NULL)

		if (cursor->data < k)
		{

			node * back = cursor->back;

			if (cursor->next != NULL)
				cursor->next->back =cursor-> back;

			if (cursor->back != NULL)
				cursor->back->next = cursor->next;
			else
				return true;

			delete cursor;
			deleteReverse(back, k);
		}

		else
			deleteReverse(cursor->back, k);

	else
		return false;
}

void displayList(node *& head)
{
	node * cursor;
	cursor = head;

	while (cursor != NULL)
	{
		cout << cursor->data << endl;
		cursor = cursor->next;
	}

}

void deleteHead(node *&head)
{
	if (head->next != NULL)
	{
		head = head->next;
		head->back = NULL;
	}
	else
		head = NULL;
}