#pragma once
#ifndef plane_h
#define plane_h
#include "A7P2_passengers.h"
#include <string>
using namespace std;
const int MAX_PlANE_CAPACITY = 600;

class plane
{
private: 

	passenger passengers[MAX_PlANE_CAPACITY];
	string airline;
	string planeNum;
	string flight;
	string details;
	int onBoard;
	int maxCapacity;

public: 
	plane();
	plane(int);
	plane(string, string, string, string, int);

	void setAirline(string);
	void setPlaneNum(string);
	void setFlight(string);
	void setDetails(string);
	void setMaxCapacity(int);
	void addPassenger(passenger);
	void clearPlane();

	string getAirline() const;
	string getPlaneNum()const;
	string getFlight() const;
	string getDetails() const;
	int getMaxCapacity() const;

	bool empty() const;
	bool full() const;

	void displayData() const;
};


#endif // !plane_h