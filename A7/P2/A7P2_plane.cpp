#include "A7P2_plane.h"
#include "A7P2_passengers.h"
#include <iostream>
#include <string>

plane::plane()
{
	airline = "";
	planeNum = "";
	flight = "";
	details = "";
	maxCapacity = 0;
	onBoard = 0;
}

plane::plane(int MaxCapacity)
{
	if (MaxCapacity>0 && MaxCapacity < MAX_PlANE_CAPACITY)
		maxCapacity = MaxCapacity;
}

plane::plane(string Airline, string PlaneNum, string Flight, string Details, int MaxCapacity)
{
	airline = Airline;
	planeNum = PlaneNum;
	flight = Flight;
	details = Details;
	maxCapacity = MaxCapacity;
	onBoard = 0;
}

void plane::setAirline(string Airline) 
{
	airline = Airline;
}

void plane::setPlaneNum(string PlaneNum)
{
	planeNum = PlaneNum;
}

void plane::setFlight(string Flight)
{
	flight = Flight;
}

void plane::setDetails(string Details)
{
	details = Details;
}

void plane::setMaxCapacity(int MaxCapacity)
{
	if (MaxCapacity>0 && MaxCapacity < MAX_PlANE_CAPACITY)
		maxCapacity = MaxCapacity;
}

void plane::addPassenger(passenger a)
{
	if (!plane::full())
	{
		passengers[onBoard] = a;
		onBoard++;
	}
}

void plane::clearPlane()
{
	for (int i = 0; i < onBoard; i++)
	{
		passengers[i] = passenger();
	}

	onBoard = 0;
}

string plane::getAirline() const
{
	return airline;
}

string plane::getPlaneNum()const
{
	return planeNum;
}

string plane::getFlight() const
{
	return flight;
}

string plane::getDetails() const
{
	return details;
}

int plane::getMaxCapacity() const
{
	return maxCapacity;
}

bool plane::empty() const
{
	return (onBoard == 0);
}

bool plane::full() const
{
	return (onBoard == maxCapacity);
}

void plane::displayData() const
{
	cout << "Airline: " << airline << endl;
	cout << "Plane: " << planeNum << endl;
	cout << "Flight: " << flight << endl;
	cout << "Passengers: " << onBoard << endl;
	cout << "Max Capacity: " << maxCapacity << endl;
	cout << "Extra Details: " << details << endl;

	if (!plane::empty())
	{
		cout << endl << endl << "PASSENGER DATA:" << endl;
		for (int i = 0; i < onBoard; i++)
		{
			cout << i + 1 << endl;
			passengers[i].displayPass();
			cout << endl;			
		}
	}

	else
	{
		cout << "NO PASSENGER DATA" << endl;
	}

}