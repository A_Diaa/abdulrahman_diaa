#pragma once
#ifndef passengers_h
#define passengers_h
#include <string>
using namespace std;

class passenger
{
private:

	string name;
	string passport;
	string nation;
	string details;
	int age;

public: 
	passenger();
	passenger(string);
	passenger(string, string, string, string, int);

	void setName(string);
	void setPassport(string);
	void setNation(string);
	void setDetails(string);
	void setAge(int);

	string getName() const;
	string getPassport() const;
	string getNation() const;
	string getDetails() const;
	int getAge() const;

	void displayPass() const;
};
#endif //!