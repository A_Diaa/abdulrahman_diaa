#include "A7P2_passengers.h"
#include <string>
#include <iostream>
using namespace std;

passenger::passenger()
{
	name = "";
	nation = "";
	passport = "";
	details = "";
	age = 0;
}

passenger::passenger(string Name)
{
	name = Name;
}

passenger::passenger(string Name, string Nation, string Passport, string Details, int Age)
{
	name = Name;
	passport = Passport;
	nation = Nation;
	details = Details;
	age = Age;
}

void passenger::setName(string Name)
{
	name = Name;
}

void passenger::setNation(string Nation)
{
	nation = Nation;
}

void passenger::setPassport(string Passport)
{
	passport = Passport;
}

void passenger::setDetails(string Details)
{
	details = Details;
}

void passenger::setAge(int Age)
{
	if (Age >0)
		age = Age;
}

string passenger::getName() const
{
	return name;
}

string passenger::getNation() const
{
	return nation;
}

string passenger::getPassport() const
{
	return passport;
}

string passenger::getDetails() const
{
	return details;
}

int passenger::getAge() const
{
	return age;
}

void passenger::displayPass() const
{
	cout << "Name: " << name << endl;
	cout << "Nationality: " << nation << endl;
	cout << "Passport Number: " << passport << endl;
	cout << "Age: " << age << endl;
	cout << "Extra Details: " << details << endl;
}