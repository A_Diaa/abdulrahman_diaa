#include "A7P2_passengers.h"
#include "A7P2_plane.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	plane P ("EgyptAir", "E14710", "6630AM", "Overnight", 4);
	passenger X("Diaa", "Egyptian","900162359", "Terrorist", 19);
	passenger Y("Abdo", "Egyptian", "900162359", "Terrorist", 19);
	passenger Z("Abdulrahman", "Egyptian", "900162359" , "Terrorist", 19);
	cout << "plane inbound : " << endl;
	P.addPassenger(X);
	P.addPassenger(Y);
	P.addPassenger(Z);
	P.displayData();
	cout << endl << endl << "CLEARING PLANE" << endl;
	P.clearPlane();
	cout << endl << endl;
	P.displayData();
	system("pause");
	return 0;
}