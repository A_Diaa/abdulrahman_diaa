#pragma once
#ifndef circle_h
#define circle_h

class circle
{
private:
	double radius;
	double circumference;
	double area;
	const float pi = 22.0 / 7.0;
public:
	circle();
	circle(double);
	void setRadius(double);
	void setCircum(double);
	void setArea(double);
	double getRadius() const;
	double getCircum() const;
	double getArea() const;
	void displayData() const;
};
#endif // !