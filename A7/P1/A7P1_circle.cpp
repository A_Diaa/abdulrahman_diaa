#include "A7P1_circle.h"
#include <iostream>
#include <cmath>
using namespace std;

circle::circle()
{
	radius = 0.0;
	circumference = 0.0;
	area = 0.0;
}

circle::circle(double R)
{
	if (R > 0.0)
	{
		radius = R;
		circumference = 2 * pi*R;
		area = pi*R*R;
	}
}

void circle::setRadius(double R)
{
	if (R > 0.0)
	{
		radius = R;
		circumference = 2 * pi*R;
		area = pi*R*R;
	}
}

void circle::setCircum(double C)
{
	if (C > 0.0)
	{
		radius = C / 2 / pi;
		circumference = C;
		area = pi*radius*radius;
	}
}

void circle::setArea(double A)
{
	if (A > 0.0)
	{
		radius = sqrt(A / pi);
		circumference = 2 * pi*radius;
		area = A;
	}
}

double circle::getArea() const
{
	return area;
}

double circle::getCircum() const
{
	return circumference;
}

double circle::getRadius() const
{
	return radius;
}

void circle::displayData() const
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Radius: " << radius << endl;
	cout << "Circumference: "  << circumference << endl;
	cout << "Area: "  << area << endl;
}