#include <iostream>
#include "A7P1_circle.h"
#include "A7P1_rectangle.h"
#include"A7P1_square.h"
using namespace std;

int main()
{
	circle x(22.0);
	rectangle y(29.0, 27.0);
	square z(32.0);

	z.displayData();
	cout << endl;
	x.displayData();
	cout << endl;
	y.displayData();
	cout << endl;

	system("pause");
	return 0;
}