#pragma once
#ifndef square_h
#define square_h
#include"A7P1_rectangle.h"
class square
{
private :
	rectangle sq;
public: 
	square();
	square(double);
	void setSide(double);
	void setArea(double);
	void setPerimeter(double);
	double getSide() const;
	double getPerimeter() const;
	double getArea() const;
	void displayData() const;
};
#endif // !