#include "A7P1_rectangle.h"
#include <iostream>
using namespace std;

rectangle::rectangle()
{
	length = 0.0;
	width = 0.0;
	perimeter = 0.0;
	area = 0.0;
}

rectangle::rectangle(double dim1, double dim2)
{
	if (dim1 > 0.0 && dim2 > 0.0)
	{
		if (dim1 > dim2)
		{
			length = dim1;
			width = dim2;
		}
		else
		{
			length = dim2;
			width = dim1;
		}

		perimeter = 2 * length*width;
		area = length*width;
	}

}

void rectangle::setLength(double L)
{
	if (L > 0.0)
	{
		length = L;
		perimeter = 2 * length*width;
		area = length*width;
	}

}

void rectangle::setWidth(double W)
{
	if (W > 0.0)
	{
		width = W;
		perimeter = 2 * length*width;
		area = length*width;
	}
}
double rectangle::getArea() const
{
	return area;
}

double rectangle::getPerimeter() const
{
	return perimeter;
}

double rectangle::getLength() const
{
	return length;
}

double rectangle::getWidth() const
{
	return width;
}

void rectangle::displayData() const
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Length: "  << length << endl;
	cout << "Width: "  << width << endl;
	cout << "Area: " << area << endl;
	cout << "Perimeter: " << perimeter << endl;
}