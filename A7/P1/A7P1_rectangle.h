#pragma once
#ifndef rectangle_h
#define rectangle_h
class rectangle
{
private: 
	double length;
	double width;
	double area;
	double perimeter;

public: 
	rectangle();
	rectangle(double, double);
	void setLength(double);
	void setWidth(double);
	double getLength() const;
	double getWidth() const;
	double getPerimeter() const;
	double getArea() const;
	void displayData() const;
};
#endif // !rectangle_h
