#include "A7P1_square.h"
#include "A7P1_rectangle.h"
#include <iostream>
#include <cmath>
using namespace std;

square::square()
{
	sq.setLength(0);
	sq.setWidth(0);
}

square::square(double side)
{
	sq.setLength(side);
	sq.setWidth(side);
}

void square::setSide(double side)
{
	sq.setLength(side);
	sq.setWidth(side);
}

void square::setArea(double area)
{
	sq.setLength(sqrt(area));
	sq.setWidth(sqrt(area));
}

void square::setPerimeter(double perimeter)
{
	sq.setLength(perimeter / 4.0);
	sq.setWidth(perimeter / 4.0);
}

double square::getSide() const
{
	return sq.getLength();
}

double square::getArea() const
{
	return sq.getArea();
}

double square::getPerimeter() const
{
	return sq.getPerimeter();
}

void square::displayData() const
{
	cout << endl;
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Side: "  << sq.getWidth() << endl;
	cout << "Area: "  << sq.getArea() << endl;
	cout << "Perimeter: "  << sq.getPerimeter() << endl;
	cout << endl;
}
