#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace std;

ifstream  input;
ofstream output;

const int rows = 3, cols = 3;

enum winner {draw, x, o};

void ReadXO (string, char [rows][cols]); //fills 2D array from file
void Display(string, char[rows][cols]); //outputs file from 2D array

int count(char, char[rows][cols]); //counts the occurence of a character in a 2D char array

bool Validate(char[rows][cols]); //checks for valid input matrix after winner decision 
bool checkWin(char[rows][cols], char); //takes x or o and determines if the letter has won

winner result(char[rows][cols]); //returns winner or draw

int main()
{
	char game[rows][cols];
	string inpath, outpath;

	cin >> inpath >> outpath;

	ReadXO(inpath, game);
	Display(outpath, game);

	if (Validate(game))
		output << result(game) << endl;

	output.close();

	system("pause");
	return 0;
}

void ReadXO(string path, char game[rows][cols])
{
	char temp;
	input.open(path.c_str());
	if (input.is_open())
	{
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
			{
				input.get(temp);

				while (temp == '\n')
					input.get(temp);

				game[i][j] = toupper(temp);
			}
	}
	input.close();
}

void Display(string path, char game[rows][cols])
{
	output.open(path.c_str());
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				output.put(game[i][j]);
			}
			output << endl;
		}
}

int count(char a, char game[rows][cols])
{
	int count=0;
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			if (game[i][j] == a)
				count++;
	return count;
}

bool Validate(char game[rows][cols])
{
	bool Xwins=checkWin(game,'X');
	bool Owins=checkWin(game,'O');

	int X = count('X', game);
	int O = count('O', game);

	if ((X - O > 1))
	{
		output << "-1" << endl;
		return false;
	}
	if ((O - X > 1))
	{
		output << "-2" << endl;
		return false;
	}

	if (Xwins&&Owins)
	{
		output << "-3" << endl;
		return false;
	}
	return true;
}

bool checkWin(char game[rows][cols], char xo)
{
	//rows
	if ((game[0][0] == xo && game[0][0] == game[0][1] && game[0][1] == game[0][2])) //row1
		return true;
	if ((game[1][0] == xo && game[1][0] == game[1][1] && game[1][1] == game[1][2])) //row2
		return true;
	if ((game[2][0] == xo && game[2][0] == game[2][1] && game[2][1] == game[2][2])) //row3
		return true;
	//columns
	if ((game[0][0] == xo && game[0][0] == game[1][0] && game[1][0] == game[2][0])) //column1
		return true;
	if ((game[0][1] == xo && game[0][1] == game[1][1] && game[1][1] == game[2][1])) //column2
		return true;
	if ((game[0][2] == xo && game[0][2] == game[1][2] && game[1][2] == game[2][2])) //column3
		return true;
	//diagonals
	if ((game[0][2] == xo && game[0][2] == game[1][1] && game[1][1] == game[2][0])) //diagonal2
		return true;
	if ((game[0][0] == xo && game[0][0] == game[1][1] && game[1][1] == game[2][2])) //diagonal1
		return true;

	return false;
}

winner result(char game[rows][cols])
{
	bool X = checkWin(game, 'X');
	bool O = checkWin(game, 'O');

	if (X)
		return x;
	if (O)
		return o;
	else
		return draw;
}