#include <iostream>
#include <string>
#include <fstream>

using namespace std;

ifstream input;

bool isFileOrdered(string);
void tolower(string&);

int main()
{
	string path;
	cin >> path;
	if (isFileOrdered(path))
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
	system("pause");
	return 0;
}

bool isFileOrdered(string filename)
{
	string temp1, temp2;
	input.open(filename.c_str());
	if (input.is_open())
	{
		input >> temp2;
		tolower(temp2);
		while (!input.eof())
		{
			temp1 = temp2;
			input >> temp2;
			tolower(temp2);
			if (!(temp2 >= temp1))
				return false;
		}
		return true;
	}
}

void tolower(string & upper)
{
	for (int i = 0; i < upper.length(); i++)
		upper[i] = tolower(upper[i]);
}