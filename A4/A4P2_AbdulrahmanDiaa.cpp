#include <iostream>
#include <fstream>
#include <string>

using namespace std;
ifstream input1, input2;

bool filesIdentical(string, string);

int main()
{
	string path1, path2;
	cin >> path1 >> path2;
	if (filesIdentical(path1, path2))
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
	system("pause");
	return 0;
}

bool filesIdentical(string path1, string path2)
{
	char char1, char2, charr1, charr2;
	input1.open(path1.c_str());
	input2.open(path2.c_str());
	if (input1.is_open() && input2.is_open())
	{
		while (!input1.eof() && !input2.eof())
		{
			input1.get(char1);
			input2.get(char2);
			charr1 = tolower(char1);
			charr2 = tolower(char2);
			if (charr1 != charr2)
				return false;
		}
		return true;
	}
}