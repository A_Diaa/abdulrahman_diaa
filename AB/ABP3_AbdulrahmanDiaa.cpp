#include <iostream>
#include <string>
using namespace std;

bool vowel(char c)
{
	char k = tolower(c);
	if (k == 'a' || k == 'e' || k == 'i' || k == 'o' || k == 'u' || k == 'y')
		return true;
	return false;
}

int main()
{
	string input;
	cin >> input;
	for (int i = 0; i < input.length(); i++)
	{
		if (isupper(input.at(i)))
			input.at(i) = tolower(input.at(i));
		if (vowel(input.at(i)))
		{
			input.erase(i, 1);
			i--;
		}
		else
		{
			input.insert(i,1, '.');
			i++;
		}
	}
	cout << input << endl;
	return 0;
}