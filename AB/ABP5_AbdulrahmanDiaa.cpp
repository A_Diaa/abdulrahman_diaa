#include <iostream> 
using namespace std;

int main()
{
	int number, height, person, width=0;
	cin >> number >> height;
	for (int i = 0; i < number; i++)
	{
		cin >> person;
		if (person > height)
			width += 2;
		else width += 1;
	}

	cout << width << endl;
	return 0;
}