#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

bool descending(int i, int j)
{
	return (i > j);
}

int main()
{
	int minimum, months=0, grown=0;
	vector <int> perMonth (12);
	cin >> minimum;
	for (int i = 0; i < 12; i++)
		cin >> perMonth[i];
	sort(perMonth.begin(), perMonth.end(), descending);

	while ((grown < minimum) && months<12)
	{
		grown += perMonth[months];
		months++;
	}

	if (grown < minimum)
		cout << "-1" << endl;
	else
		cout << months << endl;

	return 0;
}