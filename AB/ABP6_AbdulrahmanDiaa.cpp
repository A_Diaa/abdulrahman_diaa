#include <iostream>
using namespace std;

struct coordinates
{
	int x;
	int y;
};

int main()
{
	coordinates t1, t2, t3, t4;
	cin >> t1.x >> t1.y;
	cin >> t2.x >> t2.y;

	if (t2.y - t1.y!=0 && t2.y - t1.y != t2.x - t1.x && t2.x - t1.x!=0 && t2.y - t1.y != t1.x - t2.x)
		cout << "-1" << endl;
	else
	{
		if (t1.x == t2.x)
		{
			t3.y = t1.y;
			t4.y = t2.y;
			t3.x = (t1.y - t2.y) + t1.x;
			t4.x = t3.x;
		}
		else if (t1.y == t2.y)
		{
			t3.x = t1.x;
			t4.x = t2.x;
			t3.y = (t1.x - t2.x) + t1.y;
			t4.y = t3.y;

		}
		else
		{
			t3.x = t1.x;
			t3.y = t2.y;
			t4.x = t2.x;
			t4.y = t1.y;
		}
		cout << t3.x << " " << t3.y << " " << t4.x << " " << t4.y << endl;
	}
	return 0;
}