#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	int number, temp, taxis = 0;
	int groups[4] = { 0, 0, 0, 0 };
	cin >> number;
	for (int i = 0; i < number; i++)
	{
		cin >> temp;
		groups[temp - 1]++;
	}

	groups[0] -= min(groups[2], groups[0]);
	groups[0] -= min(groups[0], 2 * (groups[1] % 2));
	taxis += groups[3] + groups[2] + groups[1] / 2 + groups[1] % 2 + groups[0] / 4 + (groups[0] % 4 > 0);
	cout << taxis << endl;
	return 0;
}