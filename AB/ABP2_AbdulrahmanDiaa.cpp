#include <iostream>
#include <string>
using namespace std;

int main()
{
	string input;

	cin >> input;
	if (input.length() <= 2)
		cout << input;
	else
	{
		for (int i = 0; i < input.length() - 2; i += 2)
			for (int j = 0; j < input.length() - i - 2; j += 2)
				if (input.at(j) > input.at(j + 2))
					swap(input.at(j), input.at(j + 2));
		cout << input << endl;
	}
	return 0;
}