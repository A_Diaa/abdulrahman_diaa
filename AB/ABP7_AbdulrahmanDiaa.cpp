#include <iostream>
#include <algorithm>
#include <climits>
using namespace std;

int main()
{
	int minPrice=INT_MAX, days, weight, price, total=0;

	cin >> days; 

	for (int i = 0; i < days; i++)
	{
		cin >> weight >> price;
		minPrice = min(minPrice, price);
		total += weight*minPrice;
	}

	cout << total << endl;
	return 0;
}