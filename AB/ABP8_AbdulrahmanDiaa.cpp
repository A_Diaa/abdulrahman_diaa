#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

bool descending(int i, int j)
{
	return (i > j);
}

int main()
{
	int sum = 0, number, temp, stolen=0, coins=0; 
	vector <int> values;

	cin >> number;

	for (int i = 0; i < number; i++)
	{
		cin >> temp;
		values.push_back(temp);
		sum += temp;
	}

	sort(values.begin(), values.end(), descending);

	while (stolen <= sum - stolen)
	{
		stolen += values[coins];
		coins++;
	}

	cout << coins << endl;
	return 0;
}